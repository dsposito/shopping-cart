class Product:
    def __init__(self, name: str, sku: str, price: float) -> None:
        self.name = name
        self.sku = sku
        self.price = price
