import math

from models.cart.item import CartItem

class Cart:
    def __init__(self, tax_rate: float=0.0):
        self.items = []
        self.tax_rate = tax_rate
        self.total = 0

    def add_product(self, product, quantity):
        # Update quantity if product already exists in cart.
        has_product = False
        for item in self.items:
            if item.product == product:
                has_product = True
                item.quantity += quantity
                break

        # Add product to cart if it doesn't exist.
        if not has_product:
            self.items.append(CartItem(product, quantity))

        # Recalculate total after any change to cart.
        self.calculate_total()

    def calculate_total(self):
        # Calculate the subtotal.
        self.total = 0
        for item in self.items:
            self.total += item.product.price * item.quantity

        # Add taxes.
        self.total += self.total * self.tax_rate

        # Safely round total to 2 decimal places.
        self.total = __class__.round_half_up(self.total, 2)

    @staticmethod
    def round_half_up(value, decimals=0):
        multiplier = 10 ** decimals
        return math.floor(value*multiplier + 0.5) / multiplier
