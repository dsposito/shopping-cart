from models.product import Product

class CartItem:
    def __init__(self, product: Product, quantity):
        self.product = product
        self.quantity = quantity
