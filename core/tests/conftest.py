import pytest

from models.product import Product

@pytest.fixture(scope="module")
def product_dove():
    return Product("Dove Soap", "SOAP-DOVE", 39.99)

@pytest.fixture(scope="module")
def product_axe():
    return Product("Axe Deos", "SOAP-AXE", 99.99)
