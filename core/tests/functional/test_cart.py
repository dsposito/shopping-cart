
from models.cart import Cart

class TestCart:
    def test_add_single_product_with_single_quantity(self, product_dove):
        cart = Cart()
        cart.add_product(product_dove, 1)

        assert len(cart.items) == 1
        assert cart.items[0].product == product_dove
        assert cart.items[0].quantity == 1
        assert cart.total == product_dove.price

    def test_add_single_product_with_multiple_quantity(self, product_dove):
        cart = Cart()
        cart.add_product(product_dove, 5)
        cart.add_product(product_dove, 3)

        assert len(cart.items) == 1
        assert cart.items[0].product == product_dove
        assert cart.items[0].quantity == 8
        assert cart.total == product_dove.price * cart.items[0].quantity

    def test_add_multiple_product_with_multiple_quantity(self, product_dove, product_axe):
        cart = Cart(0.125)
        cart.add_product(product_dove, 2)
        cart.add_product(product_axe, 2)

        assert len(cart.items) == 2
        assert cart.items[0].product == product_dove
        assert cart.items[0].quantity == 2
        assert cart.items[1].product == product_axe
        assert cart.items[1].quantity == 2
        assert cart.total == Cart.round_half_up(
            (product_dove.price * cart.items[0].quantity + product_axe.price * cart.items[1].quantity) *
            (1 + cart.tax_rate),
            2
        )
