# Shopping Cart

## Setup
1. First, ensure you have Docker and Docker Compose installed.
2. Now run `make dev` from the project's root directory to build and launch the docker images.

> **NOTE:** Run `make dev-down` to shutdown the application once you're done testing.

## Tests
Run `make dev-tests` to run the test suite.

## Recommended Future Enhancements
 - Associate a user with a cart
 - Upgrade cart to separately track subtotal, tax, (shipping?) and grand total
 - Track availability of items (quantity in stock)
 - Consider using Adapter Pattern for sales tax calculations to allow for different tax laws
 - Associate a shipping / billing address with a cart (for more dynamic sales tax calculations)
