dev:
	docker-compose up -d --build

dev-down:
	docker-compose down

dev-tests:
	docker exec -it shopping-cart-core pytest -vsx
